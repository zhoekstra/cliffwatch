using System;
using System.Collections.Generic;

namespace Cliffwatch.World.Evaluations
{
    public class PlusEvaluation : Evaluation
    {
        public override string Id()
        {
            return "+";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            int initadd = 0;
            if (input.Type != VariableType.Int || !int.TryParse(input.Value, out initadd))
                return Variables.ErrorVariable($"{input.Values} is not an int - cannot add");
            int toadd = 0;
            if (!int.TryParse(parameters, out toadd))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot add");
            return new Variable()
            {
                Type = VariableType.Int,
                Values = new List<string>() {(initadd + toadd).ToString()}
            };
        }
    }

    public class SumEvaluation : Evaluation
    {
        public override string Id()
        {
            return "sum";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true, false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            int sum = 0;
            foreach (var i in input.Values)
            {
                int j;
                if (!int.TryParse(i, out j)) return Variables.ErrorVariable($"{i} is not an Int");
                sum += j;
            }

            return new Variable()
            {
                Type = VariableType.Int,
                Values = new List<string>() {sum.ToString()}
            };
        }
    }

    public class MinusEvaluation : Evaluation
    {
        public override string Id()
        {
            return "-";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            int init = 0;
            if (input.Type != VariableType.Int || !int.TryParse(input.Value, out init))
                return Variables.ErrorVariable($"{input.Values} is not an int - cannot add");
            int sub = 0;
            if (!int.TryParse(parameters, out sub))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot add");
            return new Variable()
            {
                Type = VariableType.Int,
                Values = new List<string>() {(init - sub).ToString()}
            };
        }
    }

    public class DivEvaluation : Evaluation
    {
        public override string Id()
        {
            return "/";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            if (input.Type == VariableType.Error) return input;
            if (input.IsList) return Variables.ErrorVariable($"{input.Values} is a list - cannot add");
            int init = 0;
            if (input.Type != VariableType.Int || !int.TryParse(input.Value, out init))
                return Variables.ErrorVariable($"{input.Values} is not an int - cannot add");
            int todiv = 0;
            if (!int.TryParse(parameters, out todiv))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot add");
            return new Variable()
            {
                Type = VariableType.Int,
                Values = new List<string>() {(init / todiv).ToString()}
            };
        }
    }

    public class MulEvaluation : Evaluation
    {
        public override string Id()
        {
            return "*";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            int init = 0;
            if (input.Type != VariableType.Int || !int.TryParse(input.Value, out init))
                return Variables.ErrorVariable($"{input.Values} is not an int - cannot add");
            int tomul = 0;
            if (!int.TryParse(parameters, out tomul))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot add");
            return new Variable()
            {
                Type = VariableType.Int,
                Values = new List<string>() {(init * tomul).ToString()}
            };
        }
    }

    public class ModEvaluation : Evaluation
    {
        public override string Id()
        {
            return "%";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            int init = 0;
            if (input.Type != VariableType.Int || !int.TryParse(input.Value, out init))
                return Variables.ErrorVariable($"{input.Values} is not an int - cannot add");
            int tomod = 0;
            if (!int.TryParse(parameters, out tomod))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot add");
            return new Variable()
            {
                Type = VariableType.Int,
                Values = new List<string>() {(init % tomod).ToString()}
            };
        }
    }
}