using System.Collections.Generic;

namespace Cliffwatch.World.Evaluations
{
    public abstract class Evaluation
    {
        public abstract string Id();
        public abstract ISet<VariableType> ValidTypes();
        public abstract ISet<bool> ValidListTypes();
        public abstract Variable EvaluateVariable(World w, Variable input, string parameters);

        public Variable CheckAndEvaluateVariable(World w, Variable input, string parameters)
        {
            if (input.Type == VariableType.Error) return input;
            if (!ValidTypes().Contains(input.Type))
                return Variables.ErrorVariable(
                    $"{input.Values} is of type {input.Type}, which is not a valid type for {Id()}");
            if (!ValidListTypes().Contains(input.IsList))
                return Variables.ErrorVariable($"{input.Values} is not the right List Type for {Id()}");
            return EvaluateVariable(w, input, parameters);
        }
    }
}