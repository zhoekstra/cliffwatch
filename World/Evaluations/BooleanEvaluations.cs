using System.Collections.Generic;
using System.Linq;

namespace Cliffwatch.World.Evaluations
{
    public class GreaterThanEvaluation : Evaluation
    {
        public override string Id()
        {
            return ">";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            int comp1 = 0;
            if (!int.TryParse(input.Value, out comp1))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot compare");
            int comp2 = 0;
            if (!int.TryParse(parameters, out comp2))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot compare");
            if (comp1 > comp2) return Variables.TrueVariable;
            else return Variables.FalseVariable;
        }
    }

    public class GreaterThanOrEqualEvaluation : Evaluation
    {
        public override string Id()
        {
            return ">=";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            int comp1 = 0;
            if (!int.TryParse(input.Value, out comp1))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot compare");
            int comp2 = 0;
            if (!int.TryParse(parameters, out comp2))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot compare");
            if (comp1 >= comp2) return Variables.TrueVariable;
            else return Variables.FalseVariable;
        }
    }

    public class LessThanEvaluation : Evaluation
    {
        public override string Id()
        {
            return "<";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            var comp1 = 0;
            if (!int.TryParse(input.Value, out comp1))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot compare");
            var comp2 = 0;
            if (!int.TryParse(parameters, out comp2))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot compare");
            if (comp1 < comp2) return Variables.TrueVariable;
            else return Variables.FalseVariable;
        }
    }

    public class LessThanOrEqualEvaluation : Evaluation
    {
        public override string Id()
        {
            return "<=";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            var comp1 = 0;
            if (!int.TryParse(input.Value, out comp1))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot compare");
            var comp2 = 0;
            if (!int.TryParse(parameters, out comp2))
                return Variables.ErrorVariable($"{parameters} is not an int - cannot compare");
            return comp1 <= comp2 ? Variables.TrueVariable : Variables.FalseVariable;
        }
    }

    public class IsEqualEvaluation : Evaluation
    {
        public override string Id()
        {
            return "==";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Int};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            return input.Value == parameters ? Variables.TrueVariable : Variables.FalseVariable;
        }
    }

    public class HasTriggeredEvaluation : Evaluation
    {
        public override string Id()
        {
            return "HasTriggered";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Str};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false, true};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            return input.Values.All(id => w.TriggeredEvents.Contains(id))
                ? Variables.TrueVariable
                : Variables.FalseVariable;
        }
    }

    public class NotEvaluation : Evaluation
    {
        public override string Id()
        {
            return "IsFalse";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Bool};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            return input == Variables.TrueVariable ? Variables.FalseVariable : Variables.TrueVariable;
        }
    }
}