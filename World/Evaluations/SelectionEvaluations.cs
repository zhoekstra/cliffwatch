using System;
using System.Collections.Generic;
using System.Linq;

namespace Cliffwatch.World.Evaluations
{
    public class RandomEvaluation : Evaluation
    {

        public override string Id()
        {
            return "Random";
        }

        public override ISet<VariableType> ValidTypes()
        {
            return Variable.AllTypes;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            return new Variable()
            {
                Type = input.Type,
                Values = new List<string>() {input.Values[R.Random.Next(input.Values.Count())]}
            };
        }
    }

    public class HighestViolenceEvaluation : Evaluation
    {
        public override string Id()
        {
            return "HighestViolence";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>()
            {VariableType.Street, VariableType.District, VariableType.Intersection, VariableType.Business};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            string bestid;
            switch (input.Type)
            {
                case VariableType.Street:
                    var invalidStreets = input.Values.Where(s => w.Streets.ContainsKey(s)).ToArray();
                    if (invalidStreets.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidStreets)}");
                    bestid = input.Values.Select(s => w.Streets[s]).OrderByDescending(s => s.Violence).First().Id;
                    break;
                case VariableType.District:
                    var invalidDistricts = input.Values.Where(s => w.Districts.ContainsKey(s)).ToArray();
                    if (invalidDistricts.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidDistricts)}");
                    bestid = input.Values.Select(s => w.Districts[s]).OrderByDescending(s => s.Violence).First().Id;
                    break;
                case VariableType.Intersection:
                    var invalidIntersections = input.Values.Where(s => w.Intersections.ContainsKey(s)).ToArray();
                    if (invalidIntersections.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidIntersections)}");
                    bestid = input.Values.Select(s => w.Intersections[s]).OrderByDescending(s => s.Violence).First().Id;
                    break;
                case VariableType.Business:
                    var invalidBusinesses = input.Values.Where(s => w.Businesses.ContainsKey(s)).ToArray();
                    if (invalidBusinesses.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidBusinesses)}");
                    bestid = input.Values.Select(s => w.Businesses[s]).OrderByDescending(s => s.Violence).First().Id;
                    break;
                default:
                    return Variables.ErrorVariable(
                        $"Variable {input.Values} is of type {input.Type} and does not have a violence value");
            }

            return new Variable()
            {
                Type = input.Type,
                Values = new List<string>() {bestid}
            };
        }
    }

    public class LowestViolenceEvaluation : Evaluation
    {
        public override string Id()
        {
            return "LowestViolence";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>()
            {VariableType.Street, VariableType.District, VariableType.Intersection, VariableType.Business};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            string bestid;
            switch (input.Type)
            {
                case VariableType.Street:
                    var invalidStreets = input.Values.Where(s => w.Streets.ContainsKey(s)).ToArray();
                    if (invalidStreets.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidStreets)}");
                    bestid = input.Values.Select(s => w.Streets[s]).OrderBy(s => s.Violence).First().Id;
                    break;
                case VariableType.District:
                    var invalidDistricts = input.Values.Where(s => w.Districts.ContainsKey(s)).ToArray();
                    if (invalidDistricts.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidDistricts)}");
                    bestid = input.Values.Select(s => w.Districts[s]).OrderBy(s => s.Violence).First().Id;
                    break;
                case VariableType.Intersection:
                    var invalidIntersections = input.Values.Where(s => w.Intersections.ContainsKey(s)).ToArray();
                    if (invalidIntersections.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidIntersections)}");
                    bestid = input.Values.Select(s => w.Intersections[s]).OrderBy(s => s.Violence).First().Id;
                    break;
                case VariableType.Business:
                    var invalidBusinesses = input.Values.Where(s => w.Businesses.ContainsKey(s)).ToArray();
                    if (invalidBusinesses.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidBusinesses)}");
                    bestid = input.Values.Select(s => w.Businesses[s]).OrderBy(s => s.Violence).First().Id;
                    break;
                default:
                    return Variables.ErrorVariable(
                        $"Variable {input.Values} is of type {input.Type} and does not have a violence value");
            }

            return new Variable()
            {
                Type = input.Type,
                Values = new List<string>() {bestid}
            };
        }
    }

    public class HighestVpEvaluation : Evaluation
    {
        public override string Id()
        {
            return "HighestVp";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.District};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            string bestid;
            switch (input.Type)
            {
                case VariableType.District:
                    var invalidDistricts = input.Values.Where(s => w.Districts.ContainsKey(s)).ToArray();
                    if (invalidDistricts.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidDistricts)}");
                    bestid = input.Values.Select(s => w.Districts[s]).OrderByDescending(s => s.Vp).First().Id;
                    break;
                default:
                    return Variables.ErrorVariable(
                        $"Variable {input.Values} is of type {input.Type} and does not have a vp value");
            }

            return new Variable()
            {
                Type = input.Type,
                Values = new List<string>() {bestid}
            };
        }
    }

    public class LowestVpEvaluation : Evaluation
    {
        public override string Id()
        {
            return "LowestVp";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.District};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            string bestid;
            switch (input.Type)
            {
                case VariableType.District:
                    var invalidDistricts = input.Values.Where(s => w.Districts.ContainsKey(s)).ToArray();
                    if (invalidDistricts.Any())
                        return Variables.ErrorVariable($"variables {String.Join(',', invalidDistricts)}");
                    bestid = input.Values.Select(s => w.Districts[s]).OrderBy(s => s.Vp).First().Id;
                    break;
                default:
                    return Variables.ErrorVariable(
                        $"Variable {input.Values} is of type {input.Type} and does not have a vp value");
            }

            return new Variable()
            {
                Type = input.Type,
                Values = new List<string>() {bestid}
            };
        }
    }
}