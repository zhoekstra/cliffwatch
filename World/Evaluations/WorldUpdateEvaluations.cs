using System;
using System.Collections.Generic;

namespace Cliffwatch.World.Evaluations
{
    public class SaveAsEvaluation : Evaluation
    {
        public override string Id()
        {
            return "SaveAs";
        }

        public override ISet<VariableType> ValidTypes()
        {
            return Variable.AllTypes;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            w.Variables[parameters] = new Variable()
            {
                Id = parameters,
                Type = input.Type,
                Values = input.Values
            };
            return Variables.VoidVariable();
        }
    }

    public class IncreaseViolence : Evaluation
    {
        public override string Id()
        {
            return "IncreaseViolence";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Street};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true, false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            foreach (string strid in input.Values)
            {
                if (!w.Streets.ContainsKey(strid)) return Variables.ErrorVariable($"Street {strid} does not exist");
                w.Streets[strid].IncreaseViolence();
            }

            return Variables.VoidVariable();
        }
    }

    public class DecreaseViolence : Evaluation
    {
        public override string Id()
        {
            return "DecreaseViolence";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Street};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true, false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            foreach (string strid in input.Values)
            {
                if (!w.Streets.ContainsKey(strid)) return Variables.ErrorVariable($"Street {strid} does not exist");
                w.Streets[strid].BleedViolence();
            }

            return Variables.VoidVariable();
        }
    }

    public class AddVp : Evaluation
    {
        public override string Id()
        {
            return "AddVp";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.District};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true, false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            uint parameter;
            if (!uint.TryParse(parameters, out parameter))
                return Variables.ErrorVariable($"{parameters} is not an Int");
            foreach (string dtrid in input.Values)
            {
                if (!w.Districts.ContainsKey(dtrid)) return Variables.ErrorVariable($"{dtrid} is not a District");
                w.Districts[dtrid].Vp += parameter;
            }

            return Variables.VoidVariable();
        }
    }

    public class RemoveVp : Evaluation
    {
        public override string Id()
        {
            return "RemoveVp";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.District};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            uint parameter;
            if (!uint.TryParse(parameters, out parameter))
                return Variables.ErrorVariable($"{parameters} is not an Int");
            foreach (string dtrid in input.Values)
            {
                if (!w.Districts.ContainsKey(dtrid)) return Variables.ErrorVariable($"{dtrid} is not a District");
                var district = w.Districts[dtrid];
                if (district.Vp <= parameter)
                    district.Vp = 0;
                else district.Vp -= parameter;
            }

            return Variables.VoidVariable();
        }
    }
}