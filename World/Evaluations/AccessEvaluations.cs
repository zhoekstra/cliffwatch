using System;
using System.Collections.Generic;
using System.Linq;

namespace Cliffwatch.World.Evaluations
{
    public class GetVpEvaluation : Evaluation
    {
        public override string Id()
        {
            return "Vp";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>()
            {VariableType.District, VariableType.Player};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true, false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            var invalidValues = input.Values.Where(d => !Variables.ValidateId(w, d, input.Type)).ToList();
            if (invalidValues.Any())
            {
                return Variables.ErrorVariable($"{invalidValues.First()} is not a valid {input.Type}");
            }

            var vals = input.Type == VariableType.District
                ? input.Values.Select(did => w.Districts[did].Vp.ToString())
                : input.Values.Select(did => w.Players[did].Vp.ToString());

            return new Variable()
            {
                Type = VariableType.Int,
                Values = vals.ToList()
            };
        }
    }

    public class GetViolenceEvaluation : Evaluation
    {
        public override string Id()
        {
            return "Violence";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>()
            {VariableType.Business, VariableType.District, VariableType.Intersection, VariableType.Street};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true, false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            var invalidDistricts = input.Values.Where(d => !Variables.ValidateId(w, d, input.Type)).ToList();
            if (invalidDistricts.Any())
            {
                return Variables.ErrorVariable($"{invalidDistricts.First()} is not a valid {input.Type}");
            }

            var violences = input.Values.Select(v =>
            {
                switch (input.Type)
                {
                    case VariableType.Business:
                        return w.Businesses[v].Violence.ToString();
                    case VariableType.District:
                        return w.Districts[v].Violence.ToString();
                    case VariableType.Intersection:
                        return w.Intersections[v].Violence.ToString();
                    case VariableType.Street:
                        return w.Streets[v].Violence.ToString();
                    default:
                        return "";
                }
            });

            return new Variable()
            {
                Type = VariableType.Int,
                Values = violences.Select(v => v.ToString()).ToList()
            };
        }
    }
}