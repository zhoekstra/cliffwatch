using System.Collections.Generic;

namespace Cliffwatch.World.Evaluations
{
    public class AsStreetEvaluation : Evaluation
    {
        public override string Id()
        {
            return "AsStreet";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Str};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            if (!w.Streets.ContainsKey(input.Value))
                return Variables.ErrorVariable($"VARIABLE {input.Value} IS NOT A STREET");
            return new Variable()
                {Id = "AsStreetEvaluation", Type = VariableType.Street, Values = input.Values};
        }
    }

    public class AsIntersectionEvaluation : Evaluation
    {
        public override string Id()
        {
            return "AsIntersection";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Str};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            if (!w.Intersections.ContainsKey(input.Value))
                return Variables.ErrorVariable($"VARIABLE {input.Value} IS NOT AN INTERSECTION");
            return new Variable()
            {
                Id = "AsIntersectionEvaluation", Type = VariableType.Intersection, Values = input.Values
            };
        }
    }

    public class AsDistrictEvaluation : Evaluation
    {
        public override string Id()
        {
            return "AsDistrict";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>() {VariableType.Str};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            if (!w.Districts.ContainsKey(input.Value))
                return Variables.ErrorVariable($"VARIABLE {input.Value} IS NOT A DISTRICT");
            return new Variable()
                {Id = "AsDistrictEvaluation", Type = VariableType.District, Values = input.Values};
        }
    }
}