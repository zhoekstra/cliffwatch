using System;
using System.Collections.Generic;
using System.Linq;

namespace Cliffwatch.World.Evaluations
{
    public class ListAppendEvaluation : Evaluation
    {
        public override string Id()
        {
            return "Append";
        }

        public override ISet<VariableType> ValidTypes()
        {
            return Variable.AllTypes;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true, false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            var toadd = parameters.Split(",").Select(p => Variables.GetVariable(w, p)).ToList();
            if (toadd.Any(v => v.Type != input.Type))
            {
                return Variables.ErrorVariable($"Not all values in {parameters} are of type {input.Type}");
            }

            return new Variable()
            {
                Type = input.Type,
                Values = input.Values.Union(toadd.Select(v => v.Value)).ToList()
            };
        }
    }

    public class ListSubtractEvaluation : Evaluation
    {
        public override string Id()
        {
            return "Without";
        }

        public override ISet<VariableType> ValidTypes()
        {
            return Variable.AllTypes;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {true};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            var tosub = parameters.Split(",").Select(p => Variables.GetVariable(w, p)).ToList();
            if (tosub.Any(s => s.Type != input.Type))
                return Variables.ErrorVariable($"{parameters} is not of type {input.Type}");

            var stringtosub = tosub.SelectMany(s => s.Values);

            return Variables.ListVariable("", input.Type, input.Values.ToHashSet().Except(stringtosub).ToArray());
        }
    }
}