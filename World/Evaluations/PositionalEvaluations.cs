using System;
using System.Collections.Generic;
using System.Linq;

namespace Cliffwatch.World.Evaluations
{
    public class AdjacentStreetsEvaluation : Evaluation
    {
        public override string Id()
        {
            return "AdjacentStreets";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>()
            {VariableType.Street, VariableType.District, VariableType.Intersection, VariableType.Business};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            List<string> adjStreetIds;
            switch (input.Type)
            {
                case VariableType.Street:
                    adjStreetIds = w.Streets[input.Value].AdjacentStreets.Select(s => s.Id).ToList();
                    break;
                case VariableType.District:
                    adjStreetIds = w.Districts[input.Value].AdjacentStreets.Select(s => s.Id).ToList();
                    break;
                case VariableType.Intersection:
                    adjStreetIds = w.Intersections[input.Value].AdjacentStreets.Select(s => s.Id).ToList();
                    break;
                case VariableType.Business:
                    adjStreetIds = new List<string> {w.Businesses[input.Value].Street.Id};
                    break;
                default:
                    return Variables.ErrorVariable(
                        $"Variable {input.Values} is of type {input.Type} and does not have adjacent streets");
            }

            return Variables.ListVariable("AdjacentStreetsEvaluation", VariableType.Street, adjStreetIds.ToArray());
        }
    }

    public class AdjacentDistrictsEvaluation : Evaluation
    {
        public override string Id()
        {
            return "AdjacentDistricts";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>()
            {VariableType.Street, VariableType.District, VariableType.Intersection, VariableType.Business};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            if (input.Type == VariableType.Error) return input;
            if (input.IsList) return Variables.ErrorVariable($"CANNOT SELECT ADJACENT DISTRICTS FROM LIST {input.Id}");
            List<string> adjDistrictIds;
            switch (input.Type)
            {
                case VariableType.Street:
                    adjDistrictIds = w.Streets[input.Value].AdjacentDistricts.Select(s => s.Id).ToList();
                    break;
                case VariableType.District:
                    adjDistrictIds = w.Districts[input.Value].AdjacentDistricts.Select(s => s.Id).ToList();
                    break;
                case VariableType.Intersection:
                    adjDistrictIds = w.Intersections[input.Value].AdjacentDistricts.Select(s => s.Id).ToList();
                    break;
                case VariableType.Business:
                    adjDistrictIds = new List<string> {w.Businesses[input.Value].District.Id};
                    break;
                default:
                    return Variables.ErrorVariable(
                        $"Variable {input.Value} is of type {input.Type} and does not have adjacent districts");
            }

            return Variables.ListVariable("AdjacentDistrictsEvaluation", VariableType.Street, adjDistrictIds.ToArray());
        }
    }

    public class AdjacentIntersectionsEvaluation : Evaluation
    {
        public override string Id()
        {
            return "AdjacentIntersections";
        }

        public static ISet<VariableType> ValidTypesSet = new HashSet<VariableType>()
            {VariableType.Street, VariableType.District};

        public override ISet<VariableType> ValidTypes()
        {
            return ValidTypesSet;
        }

        public static ISet<bool> ValidListTypesSet = new HashSet<bool>() {false};

        public override ISet<bool> ValidListTypes()
        {
            return ValidListTypesSet;
        }

        public override Variable EvaluateVariable(World w, Variable input, string parameters)
        {
            List<string> adjIntersectionIds;
            switch (input.Type)
            {
                case VariableType.Street:
                    adjIntersectionIds = w.Streets[input.Value].AdjacentIntersections.Select(s => s.Id).ToList();
                    break;
                case VariableType.District:
                    adjIntersectionIds = w.Districts[input.Value].AdjacentIntersections.Select(s => s.Id).ToList();
                    break;
                default:
                    return Variables.ErrorVariable(
                        $"Variable {input.Value} is of type {input.Type} and does not have adjacent intersections");
            }

            return Variables.ListVariable("AdjacentIntersectionsEvaluation", VariableType.Street,
                adjIntersectionIds.ToArray());
        }
    }
}