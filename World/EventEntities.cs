﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Cliffwatch.World;

namespace Cliffwatch.World
{
    public class Event
    {
        public string Id { get; set; }
        public double Priority { get; set; }
        public List<Requirement> Requirements { get; set; } = new List<Requirement>();
        public bool MeetsRequirements(World world) { return Requirements.All(r => r.MeetsRequirement(world)); }
        public string Headline { get; set; }
        public TextBox TextTree { get; set; }
    }

    public class TextBox
    {
        public string StoryText { get; set; }
        public string RulesText { get; set; }
        public List<Selection> Selections { get; } = new List<Selection>(); // Selected when event selected, before text box shown
        public List<WorldAction> WorldActions { get; set; } = new List<WorldAction>(); // executed when event selected, after Selections
        public PlayerSelection PlayerSelection { get; set; } = null; //If null, no player selection shown. Prompts to select 1 player
        public PlayerSelection PlayersSelection { get; set; } = null; //If null, no player selection shown. Prompts to select multiple players
        public List<TextBoxButton> Buttons { get; set; } = new List<TextBoxButton>(); // If null, button to move back to gazeteer will be shown.
    }
    
    public class Selection
    {
        public string Evaluation { get; set; }
        public string StoreAs { get; set; }
    }

    public class WorldAction
    {
        public string Evaluation { get; set; }
    }

    public class PlayerSelection
    {
        public List<WorldAction> ForEachActions { get; set;  } = new List<WorldAction>();
        public List<WorldAction> AsListActions { get; set;  } = new List<WorldAction>();
    }

    public class TextBoxButton
    {
        public string ButtonText { get; set; }
        public TextBox TextBox { get; set; }

    }
    
    public class Requirement
    {
        public string Evaluation { get; set; }

        public bool MeetsRequirement(World world)
        {
            return Variables.Evaluate(world, Evaluation) == Variables.TrueVariable;
        }
    }
}
