﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cliffwatch.World
{
    public class District
    {
        public District(string id, string name, uint vp)
        {
            this.Id = id;
            this.Name = name;
            this.Vp = vp;
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public uint Vp { get; set; }
        public Player Controller { get; set; } = null;

        public uint Violence
        {
            get => (uint)Math.Ceiling(AdjacentStreets.Select(s => (int)s.Violence).Average());
        }
        public ISet<Street> AdjacentStreets { get; } = new HashSet<Street>();

        public ISet<Intersection> AdjacentIntersections
        {
            get => AdjacentStreets.SelectMany(s => s.AdjacentIntersections).ToHashSet();
        }
        public ISet<District> AdjacentDistricts { get; } = new HashSet<District>();
        public ISet<Business> AdjacentBusinesses { get; } = new HashSet<Business>();
    }

    public class Street
    {
        public Street(string id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public uint Violence { get; set; }
        public Player Controller { get; set; } = null;

        public void IncreaseViolence()
        {
            Violence += (uint)R.Random.Next(1,4);
        }

        public void BleedViolence()
        {
            Violence -= 1;
        }
        
        public ISet<District> AdjacentDistricts { get; } = new HashSet<District>();
        public ISet<Intersection> AdjacentIntersections { get; } = new HashSet<Intersection>();
        public ISet<Business> AdjacentBusinesses { get; } = new HashSet<Business>();
        public ISet<Street> AdjacentStreets
        {
            get => AdjacentIntersections.SelectMany(i => i.AdjacentStreets).ToHashSet();
        }
    }

    public class Intersection
    {
        public Intersection(string id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
        public string Id { get; set; }
        public string Name { get; set; }

        public uint Violence
        {
            get => (uint)Math.Ceiling(AdjacentStreets.Select(s => (int)s.Violence).Average());
        }
        public ISet<Street> AdjacentStreets { get; } = new HashSet<Street>();

        public ISet<District> AdjacentDistricts
        {
            get => AdjacentStreets.SelectMany(s => s.AdjacentDistricts).ToHashSet();
        }
    }

    public enum BusinessStatus
    {
        None,
        Dead,
        Booming
    }

    public class Business
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public uint Violence
        {
            get => Street.Violence;
        }
        public District District { get; set; }
        public Street Street { get; set; }
        public BusinessStatus Status { get; set; }
    }
}
