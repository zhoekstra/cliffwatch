using System.Collections.Generic;

namespace Cliffwatch.World
{
    public enum PlayerColor
    {
        Red,
        Green,
        Blue,
        Yellow,
        Gray
    }
    public class Player
    {
        public string Id { get; set; }
        public uint Vp { get; set; } = 0;
        public string Name { get; set; }
        public PlayerColor Color { get; set; }
        public Dictionary<Faction, int> Reputation { get; } = new Dictionary<Faction, int>();
    }

    public class Faction
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<string> SetupEvaluations { get; set; } = new List<string>();
        public string SetupStoryText { get; set; }
        public string SetupRulesText { get; set; }
        public List<Event> OddTurnStandardEvents { get; set; } = new List<Event>();
        public List<Event> EventTurnStandardEvents { get; set; } = new List<Event>();
        public List<Event> Events { get; set; } = new List<Event>();
    }
}