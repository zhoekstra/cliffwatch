using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Cliffwatch.World.Evaluations;
using YamlDotNet.Core.Tokens;

namespace Cliffwatch.World
{
    public enum VariableType
    {
        Str,
        Int,
        Bool,
        Player,
        Faction,
        Street,
        District,
        Intersection,
        Error,
        Business,
        Void
    }

    public class Variable
    {
        public static ISet<VariableType> AllTypes = new HashSet<VariableType>()
        {
            VariableType.Str,
            VariableType.Int,
            VariableType.Bool,
            VariableType.Player,
            VariableType.Faction,
            VariableType.Street,
            VariableType.District,
            VariableType.Intersection,
            VariableType.Business
        };
        public string Id { get; set; } = "###UNKNOWN###";
        public VariableType Type { get; set; } = VariableType.Str;
        public bool IsList => Values.Count > 1;
        public bool isNull => Values.Count == 0;
        public string Value => Values.First();
        public List<string> Values { get; set; }

        
    }

    public static class Variables
    {

        static Variables()
        {
            // Find and load all Evaluation classes
            var baseType = typeof(Evaluation);
            foreach (var evaltype in AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => p != baseType && baseType.IsAssignableFrom(p)))
            {
                var eval = (Evaluation) Activator.CreateInstance(evaltype);
                Evaluations[eval.Id()] = eval;
            }
        }

        public static Regex VariableMatchRegex = new Regex(@"{(.*?)}|\[(.*?)\]");
        public static Regex EvaluationRegex = new Regex(@"^(.*?)(?:\((.*?)\))?$");

        public static Dictionary<string, Evaluation> Evaluations { get; } =
            new Dictionary<string, Evaluation>();

        public static string EvaluateTextString(World w, string s)
        {
            var evaluator = new MatchEvaluator {World = w};
            return VariableMatchRegex.Replace(s, evaluator.EvaluateMatch);
        }

        public static Variable Evaluate(World w, string s)
        {
            var t = s.Split(".", StringSplitOptions.RemoveEmptyEntries);
            var variable = Variables.GetVariable(w, t[0]);
            return Variables.EvaluateVariable(w, variable, t[1..]);
        }

        public static Variable GetVariable(World w, string id)
        {
            if (w.Variables.ContainsKey(id)) return w.Variables[id];
            else if (w.Players.ContainsKey(id))
                return new Variable() {Type = VariableType.Player, Values = new List<string>() {id}};
            else if (w.Factions.ContainsKey(id))
                return new Variable() {Type = VariableType.Faction, Values = new List<string>() {id}};
            else if (w.Districts.ContainsKey(id))
                return new Variable() {Type = VariableType.District, Values = new List<string>() {id}};
            else if (w.Streets.ContainsKey(id))
                return new Variable() {Type = VariableType.Street, Values = new List<string>() {id}};
            else if (w.Businesses.ContainsKey(id))
                return new Variable() {Type = VariableType.Business, Values = new List<string>() {id}};
            else if (w.Intersections.ContainsKey(id))
                return new Variable() {Type = VariableType.Intersection, Values = new List<string>() {id}};
            else if(int.TryParse(id, out var intresult))
                return new Variable() {Type = VariableType.Intersection, Values = new List<string>() {id}};
            else if (bool.TryParse(id, out var boolresult))
                return boolresult ? TrueVariable : FalseVariable;
            else
                return new Variable()
                {
                    Type = VariableType.Str,
                    Values = new List<string>() {id}
                };
        }

        public static string GetVariableName(World w, Variable variable)
        {
            var values = variable.Values
                .Select(s => s.Trim())
                .Select(s =>
                {
                    switch (variable.Type)
                    {
                        case VariableType.Player: return w.Players[s].Name;
                        case VariableType.Faction: return w.Factions[s].Name;
                        case VariableType.District: return w.Districts[s].Name;
                        case VariableType.Street: return w.Streets[s].Name;
                        case VariableType.Intersection: return w.Intersections[s].Name;
                        case VariableType.Business: return w.Businesses[s].Name;
                        default: return variable.Value;
                    }
                })
                .ToArray();
            switch (values.Count())
            {
                case 0: return "";
                case 1: return values[0];
                case 2: return $"{values[0]} and {values[1]}";
                default:
                    return string.Join(", ", values[..^1]) + $", and {values[^1]}";
            }

        }

        public static Variable EvaluateVariable(World w, Variable v, params string[] evaluations)
        {
            var variable = v;
            foreach (var eval in evaluations)
            {
                var match = EvaluationRegex.Match(eval);
                var name = match.Groups[1].Value;
                var arguments = match.Groups[2].Value;
                if (!Variables.Evaluations.ContainsKey(name))
                    return Variables.ErrorVariable($"Could not find evaluation type {name}");
                variable = Variables.Evaluations[name].CheckAndEvaluateVariable(w, variable, arguments);
            }

            return variable;
        }

        public static Variable ErrorVariable(string errorMessage)
        {
            return new Variable()
            {
                Id = errorMessage,
                Type = VariableType.Error,
                Values = new List<string>() {$"###{errorMessage}###"}
            };
        }

        public static Variable ListVariable(string id, VariableType type, params string[] values)
        {
            return new Variable()
            {
                Id = id,
                Type = type,
                Values = new List<string>(values)
            };
        }

        public static Variable TrueVariable = new Variable()
        {
            Type = VariableType.Bool,
            Values = new List<string>() {"true"}
        };
        public static Variable FalseVariable = new Variable()
        {
            Type = VariableType.Bool,
            Values = new List<string>() {"false"}
        };

        public static Variable VoidVariable()
        {
            return new Variable()
            {
                Type = VariableType.Void,
                Values = new List<string>() {""}
            };
        }

        public static bool ValidateId(World w, string id, VariableType t)
        {
            switch (t)
            {
                case VariableType.Business:
                    return w.Businesses.ContainsKey(id);
                case VariableType.District:
                    return w.Districts.ContainsKey(id);
                case VariableType.Faction:
                    return w.Factions.ContainsKey(id);
                case VariableType.Intersection:
                    return w.Intersections.ContainsKey(id);
                case VariableType.Player:
                    return w.Players.ContainsKey(id);
                case VariableType.Street:
                    return w.Streets.ContainsKey(id);
                default:
                    return false;
            }
        }
    }

    public class MatchEvaluator
    {
        public World World { get; set; }

        public string EvaluateMatch(Match m)
        {
            if (m.Groups[1].Success) //The match is a basic variable
            {
                var key = m.Groups[1].Value;
                return Variables.GetVariableName(World, Variables.GetVariable(World, key));
            }

            if (m.Groups[2].Success) //The match is an evaluated variable in the form Variable::EvaluationFunction::EvaluationFunction
            {
                return Variables.GetVariableName(World, 
                    Variables.Evaluate(World, m.Groups[2].Value));
            }

            return "### MatchEvaluator Logic Error ###";
        }
    }
}