﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cliffwatch.Records;
using YamlDotNet.Serialization;

namespace Cliffwatch.World
{
    public static class R
    {
        public static Random Random = new Random();
    }
    public class WorldStartSettings
    {
        public Dictionary<PlayerColor, string> Players { get; set; }
        public ISet<string> FactionFiles { get; set; }
    }

    public static class WorldGenerator
    {
        public static World GenerateWorld(string filePath, WorldStartSettings settings, List<string> eventFiles)
        {
            var deserializer = new Deserializer();
            var reader = new StreamReader(filePath);
            var worldRecord = deserializer.Deserialize<WorldRecord>(reader);
            var eventLists = eventFiles
                .SelectMany(ef => deserializer.Deserialize<List<EventRecord>>(new StreamReader(ef))).ToList();
            var factions = settings.FactionFiles
                .Select(ff => deserializer.Deserialize<FactionRecord>(new StreamReader(ff))).ToList();
            var factionTieEventRecords = Directory.GetFiles("C:\\src\\cliffwatch\\yaml\\faction_tie_events")
                .ToDictionary(f => Path.GetFileName(f), f => deserializer.Deserialize<List<EventRecord>>(f));
            return GenerateWorld(worldRecord, settings.Players, factions, eventLists, factionTieEventRecords);
        }

        public static World GenerateWorld(
            WorldRecord record,
            Dictionary<PlayerColor, string> players,
            List<FactionRecord> factions,
            List<EventRecord> events,
            Dictionary<string, List<EventRecord>> factionTieEventRecords)
        {
            var world = new World();
            foreach (var street in record.Streets)
            {
                world.AddStreet(street);
            }

            foreach (var district in record.Districts)
            {
                world.AddDistrict(district);
            }

            foreach (var connection in record.DistrictConnections)
            {
                world.ConnectDistricts(connection);
            }

            foreach (var intersection in record.Intersections)
            {
                world.AddIntersection(intersection);
            }

            foreach (var business in record.Businesses)
            {
                world.AddBusiness(business);
            }

            foreach (var e in events)
            {
                var eventEntity = World.GenerateEvent(e);
                world.Events[eventEntity.Id] = eventEntity;
            }

            foreach (var frecord in factions)
            {
                var factionEntity = new Faction
                {
                    Id = frecord.Id,
                    Name = frecord.Name,
                    SetupEvaluations = frecord.SetupEvaluations,
                    SetupStoryText = frecord.SetupStoryText,
                    SetupRulesText = frecord.SetupRulesText,
                    EventTurnStandardEvents = frecord.EventTurnStandardEvents.Select(World.GenerateEvent).ToList(),
                    OddTurnStandardEvents = frecord.OddTurnStandardEvents.Select(World.GenerateEvent).ToList(),
                    Events = frecord.Events.Select(World.GenerateEvent).ToList()
                };
                foreach (var otherfaction in world.Factions.Values)
                {
                    var eventsToLoad = factionTieEventRecords.ContainsKey($"{factionEntity.Id}_{otherfaction.Id}.yaml")
                        ? factionTieEventRecords[$"{factionEntity.Id}_{otherfaction.Id}.yaml"]
                        : factionTieEventRecords.ContainsKey($"{otherfaction.Id}_{factionEntity.Id}.yaml")
                            ? factionTieEventRecords[$"{otherfaction.Id}_{factionEntity.Id}.yaml"]
                            : null;
                    if (eventsToLoad == null) continue;
                    foreach (var e in eventsToLoad)
                    {
                        if (world.Events.ContainsKey(e.Id)) continue;
                        var eventEntity = World.GenerateEvent(e);
                        world.Events[eventEntity.Id] = eventEntity;
                    }
                }

                foreach (var e in factionEntity.Events)
                {
                    world.Events[e.Id] = e;
                }
                world.Factions[factionEntity.Id] = factionEntity;
            }

            foreach (var p in players)
            {
                var player = new Player
                {
                    Id = $"player_{p.Key}",
                    Color = p.Key,
                    Name = p.Value,
                    Vp = 0
                };
                world.Players[player.Id] = player;
            }

            world.SeedBusinesses(record);

            world.Variables["AllStreets"] =
                Variables.ListVariable("AllStreets", VariableType.Street,
                    world.Streets.Keys.ToArray());
            world.Variables["AllDistricts"] =
                Variables.ListVariable("AllDistricts", VariableType.District,
                    world.Districts.Keys.ToArray());
            world.Variables["AllIntersections"] =
                Variables.ListVariable("AllIntersections", VariableType.Intersection,
                    world.Intersections.Keys.ToArray());
            world.Variables["AllBusinesses"] =
                Variables.ListVariable("AllBusinesses", VariableType.Business,
                    world.Businesses.Keys.ToArray());
            world.Variables["AllPlayers"] =
                Variables.ListVariable("AllPlayers", VariableType.Player,
                    world.Players.Keys.ToArray());
            world.Variables["AllFactions"] =
                Variables.ListVariable("AllFactions", VariableType.Faction,
                    world.Factions.Keys.ToArray());
            return world;
        }
    }

    public class World
    {
        public uint Turn { get; set; } = 1;
        public Dictionary<string, District> Districts { get; } = new Dictionary<string, District>();
        public Dictionary<string, Street> Streets { get; } = new Dictionary<string, Street>();
        public Dictionary<string, Business> Businesses { get; } = new Dictionary<string, Business>();
        public Dictionary<string, Intersection> Intersections { get; } = new Dictionary<string, Intersection>();
        public Dictionary<string, Player> Players { get; } = new Dictionary<string, Player>();
        public Dictionary<string, Faction> Factions { get; } = new Dictionary<string, Faction>();
        public Dictionary<string, Variable> Variables { get; } = new Dictionary<string, Variable>();
        public Dictionary<string, Event> Events { get; } = new Dictionary<string, Event>();
        public HashSet<string> TriggeredEvents { get; } = new HashSet<string>();

        public void SeedBusinesses(WorldRecord record)
        {
            if (record.DeadBusinessCount + record.BoomingBusinessCount > Businesses.Count)
            {
                throw new ArgumentException("Not enough Businesses for specified fortune tokens");
            }

            var shuffledbusinesses = Businesses.Values.OrderBy(b => R.Random.NextDouble()).ToList();
            foreach (var b in shuffledbusinesses.Take((int) record.DeadBusinessCount))
                b.Status = BusinessStatus.Dead;
            foreach (var b in shuffledbusinesses.Skip((int) record.DeadBusinessCount)
                .Take((int) record.BoomingBusinessCount))
                b.Status = BusinessStatus.Booming;
        }

        public void AddStreet(StreetRecord record)
        {
            if (Streets.ContainsKey(record.Id))
            {
                throw new ArgumentException($"Street Id {record.Id} already exists");
            }

            Streets[record.Id] = new Street(record.Id, record.Name);
        }

        public void AddDistrict(DistrictRecord record)
        {
            if (Districts.ContainsKey(record.Id))
            {
                throw new ArgumentException($"District Id {record.Id} already exists");
            }

            var district = new District(record.Id, record.Name, record.Vp);
            record.StreetIds.ForEach(s =>
            {
                if (!Streets.ContainsKey(s)) throw new ArgumentException($"{s} is not a valid street id");
                district.AdjacentStreets.Add(Streets[s]);
            });
            Districts[record.Id] = district;
        }

        public void AddIntersection(IntersectionRecord record)
        {
            if (Intersections.ContainsKey(record.Id))
            {
                throw new ArgumentException($"Intersection Id {record.Id} already exists");
            }

            foreach (var streetId in record.StreetIds)
            {
                if (!Streets.ContainsKey(streetId))
                {
                    throw new ArgumentException($"Street Id {streetId} does not exist");
                }

                if (Streets[streetId].AdjacentIntersections.Count >= 2)
                {
                    throw new ArgumentException($"Street Id {streetId} is already attached to two intersections");
                }
            }

            var intersection = new Intersection(record.Id, record.Name);
            Intersections[record.Id] = intersection;
            foreach (var streetId in record.StreetIds)
            {
                var street = Streets[streetId];
                street.AdjacentIntersections.Add(intersection);
                intersection.AdjacentStreets.Add(street);
            }
        }

        public void AddBusiness(BusinessRecord record)
        {
            if (Businesses.ContainsKey(record.Id))
            {
                throw new ArgumentException($"Business Id {record.Id} already exists");
            }

            if (!Streets.ContainsKey(record.StreetId))
            {
                throw new ArgumentException($"Street Id {record.StreetId} does not exist");
            }

            if (!Districts.ContainsKey(record.DistrictId))
            {
                throw new ArgumentException($"District Id {record.StreetId} does not exist");
            }

            var street = Streets[record.StreetId];
            var district = Districts[record.DistrictId];
            var business = new Business
            {
                Id = record.Id,
                Name = record.Id,
                District = district,
                Street = street,
                Status = BusinessStatus.None
            };
            street.AdjacentBusinesses.Add(business);
            district.AdjacentBusinesses.Add(business);
            Businesses[record.Id] = business;
        }

        public void ConnectDistricts(DistrictConnectionRecord record)
        {
            if (!Streets.ContainsKey(record.StreetId))
            {
                throw new ArgumentException($"Street Id {record.StreetId} does not exist");
            }

            if (Streets[record.StreetId].AdjacentDistricts.Count > 0)
            {
                throw new ArgumentException(
                    $"Street Id {record.District2Id} has already been connected to two districts");
            }

            if (!Districts.ContainsKey(record.District1Id))
            {
                throw new ArgumentException($"District Id {record.District1Id} does not exist");
            }

            if (!Districts.ContainsKey(record.District2Id))
            {
                throw new ArgumentException($"District Id {record.District2Id} does not exist");
            }

            if (Districts[record.District1Id].AdjacentDistricts.Contains(Districts[record.District2Id]))
            {
                throw new ArgumentException(
                    $"District Id {record.District1Id} is already connected to District Id {record.District2Id}");
            }

            if (Districts[record.District2Id].AdjacentDistricts.Contains(Districts[record.District1Id]))
            {
                throw new ArgumentException(
                    $"District Id {record.District2Id} is already connected to District Id {record.District1Id}");
            }

            // Actually connect both districts
            var district1 = Districts[record.District1Id];
            var district2 = Districts[record.District2Id];
            var street = Streets[record.StreetId];

            district1.AdjacentDistricts.Add(district2);
            district1.AdjacentStreets.Add(street);
            district2.AdjacentDistricts.Add(district1);
            district2.AdjacentStreets.Add(street);
        }

        public static void GenerateAndAddEvents(World w, string filePath)
        {
            var reader = new StreamReader(filePath);
            var deserializer = new Deserializer();
            var eventRecords = deserializer.Deserialize<List<EventRecord>>(reader);
            eventRecords.ForEach(r =>
            {
                var ev = GenerateEvent(r);
                w.Events[ev.Id] = ev;
            });
        }

        public static Event GenerateEvent(EventRecord record)
        {
            return new Event
            {
                Id = record.Id,
                Priority = record.Priority + R.Random.NextDouble(),
                Requirements = record.Requirements.Select(GenerateRequirement).ToList(),
                Headline = record.Headline,
                TextTree = GenerateTextBox(record.TextTree)
            };
        }

        public static Requirement GenerateRequirement(RequirementRecord record)
        {
            return new Requirement
            {
                Evaluation = record.Evaluation
            };
        }

        public static TextBox GenerateTextBox(TextBoxRecord record)
        {
            return new TextBox
            {
                StoryText = record.StoryText,
                RulesText = record.RulesText,
                WorldActions = record.Actions.Select(a => new WorldAction {Evaluation = a}).ToList(),
                PlayerSelection = new PlayerSelection
                {
                    ForEachActions = record.PlayerSelection.ForEachActions
                        .Select(a => new WorldAction {Evaluation = a}).ToList(),
                    AsListActions = record.PlayerSelection.AsListActions.Select(a => new WorldAction {Evaluation = a})
                        .ToList()
                },
                PlayersSelection = new PlayerSelection
                {
                    ForEachActions = record.PlayersSelection.ForEachActions
                        .Select(a => new WorldAction {Evaluation = a}).ToList(),
                    AsListActions = record.PlayersSelection.AsListActions
                        .Select(a => new WorldAction {Evaluation = a}).ToList()
                },
                Buttons = record.Buttons.Select(GenerateTextBoxButton).ToList()
            };
        }

        public static TextBoxButton GenerateTextBoxButton(TextBoxButtonRecord record)
        {
            return new TextBoxButton
            {
                ButtonText = record.ButtonText,
                TextBox = GenerateTextBox(record.TextBox)
            };
        }

        public static List<Event> SelectEvents(World w, uint numEvents)
        {
            return w.Events
                .Where(e => !w.TriggeredEvents.Contains(e.Value.Id) && e.Value.MeetsRequirements(w))
                .OrderByDescending(e => e.Value.Priority)
                .Take((int) numEvents)
                .Select(e => e.Value)
                .ToList();
        }
    }
}