using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cliffwatch.Renderer;
using Cliffwatch.World;

namespace Cliffwatch.Controller
{
    public class EventController
    {
        public World.World World { get; set; }

        public void StartAndRunGame(IRenderer renderer)
        {
            var worldStartSettings = renderer.StartGame("C:\\src\\cliffwatch\\yaml\\factions");
            var numplayers = worldStartSettings.Players.Count;
            var validmaps = Directory.GetFiles($"C:\\src\\cliffwatch\\yaml\\maps\\{numplayers}players");
            var selectedmap = validmaps[R.Random.Next(validmaps.Length)];
            var selectedeventfiles = Directory.GetFiles("C:\\src\\cliffwatch\\yaml\\events")
                .OrderBy(f => R.Random.NextDouble()).Take(3).ToList();
            World = WorldGenerator.GenerateWorld(selectedmap, worldStartSettings, selectedeventfiles);

            do
            {
                
            } while (true);
        }
    }
}