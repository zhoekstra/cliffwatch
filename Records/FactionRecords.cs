using System.Collections.Generic;

namespace Cliffwatch.Records
{
    public class FactionRecord
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<string> SetupEvaluations { get; set; } = new List<string>();
        public string SetupStoryText { get; set; }
        public string SetupRulesText { get; set; }
        public List<EventRecord> OddTurnStandardEvents { get; set; } = new List<EventRecord>();
        public List<EventRecord> EventTurnStandardEvents { get; set; } = new List<EventRecord>();
        public List<EventRecord> Events { get; set; } = new List<EventRecord>();
        public Dictionary<string, string> FactionIdToCommonEvents { get; set; }
    }
}