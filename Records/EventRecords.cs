using System.Collections.Generic;

namespace Cliffwatch.Records
{
    public class EventRecord
    {
        public string Id { get; set; }
        public double Priority { get; set; } = 0;
        public List<RequirementRecord> Requirements { get; set; } = new List<RequirementRecord>();
        public string Headline { get; set; }
        public TextBoxRecord TextTree { get; set; }
    }

    public class TextBoxRecord
    {
        public string StoryText { get; set; }
        public string RulesText { get; set; }
        public List<string> Actions { get; set; }
        public PlayerSelectionRecord PlayerSelection { get; set; }
        public PlayerSelectionRecord PlayersSelection { get; set; }
        public List<TextBoxButtonRecord> Buttons { get; set; }
    }

    public class PlayerSelectionRecord
    {
        public List<string> ForEachActions { get; set; }
        public List<string> AsListActions { get; set; }
    }
    
    

    public class RequirementRecord
    {
        public string Evaluation { get; set; }
    }

    public class TextBoxButtonRecord
    {
        public string ButtonText { get; set; }
        public TextBoxRecord TextBox { get; set; }
    }
}