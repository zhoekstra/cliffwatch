using System.Collections.Generic;

namespace Cliffwatch.Records
{
    public class WorldRecord
    {
        public string MapImagePath { get; set; }
        public List<StreetRecord> Streets { get; set; }
        public List<DistrictRecord> Districts { get; set; }
        public List<DistrictConnectionRecord> DistrictConnections { get; set; }
        public List<BusinessRecord> Businesses { get; set; }
        public uint DeadBusinessCount { get; set; }
        public uint BoomingBusinessCount { get; set; }
        public List<IntersectionRecord> Intersections { get; set; }
    }

    public class StreetRecord
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class DistrictRecord
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public uint Vp { get; set; }
        public List<string> StreetIds { get; set; }
    }

    public class DistrictConnectionRecord
    {
        public string District1Id { get; set; }
        public string District2Id { get; set; }
        public string StreetId { get; set; }
    }

    public class IntersectionRecord
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<string> StreetIds { get; set; }
    }

    public class BusinessRecord
    {
        public string Id { get; set; }
        public string DistrictId { get; set; }
        public string StreetId { get; set; }
    }
}