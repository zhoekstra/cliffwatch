using System.Collections.Generic;
using Cliffwatch.World;
using NUnit.Framework;

namespace Cliffwatch.UnitTests
{
    public class VariableUnitTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestBasicVariables()
        {
            var w = new World.World();
            w.Variables["testvariable"] = new Variable() {Id = "testvariable", Type = VariableType.Str, Values = new List<string>() {"Hello World"}};;
            w.Variables["other"] = new Variable()
                {Id = "other", Type = VariableType.Str, Values = new List<string>() {"How Are You?"}};
            Assert.AreEqual(Variables.EvaluateTextString(w,"{testvariable} should be Hello World, {other}"),
                "Hello World should be Hello World, How Are You?");
        }

        [Test]
        public void TestRandomStreet()
        {
            var w = new World.World();
            var street1 = new Street("street1", "Apple Street");
            var street2 = new Street("street2", "Bakers Way");
            var street3 = new Street("street3", "Carters Walk");
            var intersection = new Intersection("intersection1", "Delta Square");
            var district = new District("district1", "The Shallows", 1);
            // set up adjacencies (Should replace this test with a loaded world file)
            street1.AdjacentIntersections.Add(intersection);
            intersection.AdjacentStreets.Add(street1);
            street2.AdjacentIntersections.Add(intersection);
            intersection.AdjacentStreets.Add(street2);
            street3.AdjacentIntersections.Add(intersection);
            intersection.AdjacentStreets.Add(street3);
            street1.AdjacentDistricts.Add(district);
            district.AdjacentStreets.Add(street1);
            street2.AdjacentDistricts.Add(district);
            district.AdjacentStreets.Add(street2);
            // add everything to world
            w.Streets[street1.Id] = street1;
            w.Streets[street2.Id] = street2;
            w.Streets[street3.Id] = street3;
            w.Intersections[intersection.Id] = intersection;
            w.Districts[district.Id] = district;

            Assert.AreEqual(Variables.EvaluateTextString(w, "Place a piece on [street1.AdjacentStreets]"), 
                "Place a piece on Apple Street, Bakers Way, and Carters Walk");
            Assert.AreEqual(Variables.EvaluateTextString(w, "Place a piece on [intersection1.AdjacentStreets]"),
                "Place a piece on Apple Street, Bakers Way, and Carters Walk");
            Assert.AreEqual(Variables.EvaluateTextString(w, "Place a piece on [district1.AdjacentStreets]"),
                "Place a piece on Apple Street and Bakers Way");
        }
    }
}