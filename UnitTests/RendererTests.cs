using System;
using Cliffwatch.Renderer;
using Cliffwatch.World;
using NUnit.Framework;
using System.Collections.Generic;

namespace Cliffwatch.UnitTests
{
    public class RendererTests
    {
        [Test]
        public void testLoadWorld()
        {
            var world = WorldGenerator.GenerateWorld("C:\\src\\cliffwatch\\yaml\\maps\\2player.yaml");
            world.Streets["str_robespierre"].IncreaseViolence();
            for (int i = 0; i < 100; ++i)
            {
                Console.Out.WriteLine(Variables.Evaluate(world, "dis_shades.AdjacentStreets.Random").Values);
            }
        }

        [Test()]
        public void TestRenderer()
        {
            var console = new TestConsoleWrapper();
            console.LinesToRead.Add("Ferdinand,Joey");
            new ConsoleRenderer(console).RenderTextBox(
                "Headline",
                "Some Story Text",
                "Some Rules Text",
                new string[] {"Ferdinand", "Joey"},
                false,
                true,
                new string[] {}
            );
        }
    }
}