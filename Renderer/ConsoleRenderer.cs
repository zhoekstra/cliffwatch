using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cliffwatch.Records;
using Cliffwatch.World;
using YamlDotNet.Serialization;

namespace Cliffwatch.Renderer
{
    public interface IRenderer
    {
        WorldStartSettings StartGame(string factionfolderpath);
        uint RenderGazette(string date, List<(string, bool)> events); // returns which event was selected 

        (uint, string, string[]) RenderTextBox(
            string headline,
            string storyText,
            string rulesText,
            string[] players,
            bool presentSinglePlayerChoice,
            bool presentMultiPlayerChoice,
            string[] buttons); // return null if no button selected
    }

    public class ConsoleRenderer : IRenderer
    {
        public ConsoleRenderer(IConsole console)
        {
            this.MyConsole = console;
        }

        private IConsole MyConsole { get; set; }

        public static string AsHeadline(string s)
        {
            int numDashes = s.Length == 0 ? 39 : (80 - 4 - s.Length) / 2;
            string o = s.Length == 0 ? "" : $" {s} ";
            return $"+{new String('=', numDashes)}{o}{new String('=', numDashes)}+";
        }

        public WorldStartSettings StartGame(string factionfolderpath)
        {
            MyConsole.WriteLine(AsHeadline("Welcome To Cliffwatch, City Of Shadows"));
            Dictionary<PlayerColor, string> players = new Dictionary<PlayerColor, string>();
            ISet<string> factionFiles = new HashSet<string>();
            MyConsole.WriteLine("Enter each player color, then that player's name. (ex: Green,Rachel)");
            MyConsole.WriteLine("When you are done, type 'Done'");
            while(true)
            {
                var line = MyConsole.ReadLine().Split(',').Select(s => s.Trim()).ToArray();
                if (line.Length == 1 && line[0].ToLower() == "done") break;
                if (line.Length == 2 && PlayerColor.TryParse(line[0], out PlayerColor color))
                {
                    MyConsole.WriteLine($"Added player {line[1]}, playing as {color}");
                    players[color] = line[1];
                }
                else
                {
                    MyConsole.WriteLine("not valid input - try again");
                }
            }
            
            var deserializer = new Deserializer();
            var knownFactions = Directory.GetFiles(factionfolderpath).Where(f => f.EndsWith(".yaml"))
                .ToDictionary(f => deserializer.Deserialize<FactionRecord>(new StreamReader(f)).Name, f => f);
            MyConsole.WriteLine("Pick two of these factions, separated by a comma");
            foreach (var faction in knownFactions)
            {
                MyConsole.WriteLine(faction.Key);
            }

            while (true)
            {
                var line = MyConsole.ReadLine().Split(',').Select(s => s.Trim()).ToArray();
                if (line.Length == 2 && knownFactions.ContainsKey(line[0]) && knownFactions.ContainsKey(line[1]))
                {
                    factionFiles.Add(knownFactions[line[0]]);
                    factionFiles.Add(knownFactions[line[1]]);
                    break;
                }
                MyConsole.WriteLine("Invalid Input, try again");
            }

            return new WorldStartSettings()
            {
                Players = players,
                FactionFiles = factionFiles
            };
        }

        public uint RenderGazette(string date, List<(string, bool)> events)
        {
            MyConsole.WriteLine(AsHeadline($"Cliffwatch Gazzette: {date}"));
            var eventlines = events.Select((ev, index) =>
            {
                var selectedstring = ev.Item2 ? "**" : "";
                return $"{index + 1}) {selectedstring}{ev.Item1}{selectedstring}";
            });
            foreach (var el in eventlines)
            {
                MyConsole.WriteLine(el);
            }
            MyConsole.WriteLine(AsHeadline(""));
            MyConsole.WriteLine("");
            MyConsole.WriteLine("Select an event to resolve:");
            while (true)
            {
                if (uint.TryParse(MyConsole.ReadLine(), out var selection) && selection > 0 && selection <= events.Count)
                    return selection;
                else MyConsole.WriteLine("Try Again");
            }
        }

        public (uint, string, string[]) RenderTextBox(
            string headline, 
            string storyText, 
            string rulesText,
            string[] players,
            bool presentSinglePlayerChoice, 
            bool presentMultiPlayerChoice, 
            string[] buttons)
        {
            MyConsole.WriteLine(AsHeadline($"{headline}"));
            if (!string.IsNullOrEmpty(storyText))
            {
                MyConsole.WriteLine(storyText);
                MyConsole.WriteLine("");
            }

            if (!string.IsNullOrEmpty(rulesText))
            {
                MyConsole.WriteLine(rulesText);
                MyConsole.WriteLine("");
            }

            if (presentSinglePlayerChoice)
            {
                MyConsole.WriteLine("Select One Of:");
                foreach (var playerstring in players.Select((pl, index) => $"{index}) {pl}"))
                {
                    MyConsole.WriteLine(playerstring);
                }

                MyConsole.WriteLine("");
            }

            if (presentMultiPlayerChoice)
            {
                MyConsole.WriteLine("Select Any Of:");
                foreach (var playerstring in players.Select((pl, index) => $"{index}) {pl}"))
                {
                    MyConsole.WriteLine(playerstring);
                }

                MyConsole.WriteLine("");
            }

            if (buttons != null && buttons.Any())
            {
                MyConsole.WriteLine("Choose:");
                foreach (var buttonstring in buttons.Select((bu, index) => $"{index}) {bu}"))
                {
                    MyConsole.WriteLine(buttonstring);
                }

                MyConsole.WriteLine("");
            }

            string singlePlayerSelection = null;
            if (presentSinglePlayerChoice)
            {
                do
                {
                    MyConsole.WriteLine("Select One Player:");
                    singlePlayerSelection = MyConsole.ReadLine();
                } while (!players.Contains(singlePlayerSelection));
            }

            string[] multiPlayerSelection = { };
            if (presentMultiPlayerChoice)
            {
                do
                {
                    MyConsole.WriteLine("Select Any number of players, seperated by ,'s");
                    multiPlayerSelection = MyConsole.ReadLine()?.Split(",").Select(s => s.Trim()).ToArray();
                } while (!multiPlayerSelection.All(players.Contains));
            }

            uint buttonSelection = uint.MaxValue;
            if (buttons != null && buttons.Any())
            {
                do
                {
                    MyConsole.WriteLine("Choose...");
                } while (uint.TryParse(MyConsole.ReadLine(), out buttonSelection) && buttonSelection > 0 &&
                         buttonSelection <= buttons.Length);
            }
            else if (!presentSinglePlayerChoice && !presentMultiPlayerChoice)
            {
                MyConsole.WriteLine("Press Enter To Continue...");
                MyConsole.ReadLine();
            }

            return (buttonSelection - 1, singlePlayerSelection, multiPlayerSelection);
        }
    }
    
    public interface IConsole
    {
        void Write(string message);
        void WriteLine(string message);
        string ReadLine();
    }
    
    public class ConsoleWrapper : IConsole
    {
        public void Write(string message)
        {
            Console.Write(message);
        }

        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
    
    public class TestConsoleWrapper : IConsole
    {
        public List<String> LinesToRead = new List<String>();

        public void Write(string message)
        {
            Console.Out.Write(message);
        }

        public void WriteLine(string message)
        {
            Console.Out.WriteLine(message);
        }

        public string ReadLine()
        {
            string result = LinesToRead[0];
            LinesToRead.RemoveAt(0);
            return result;
        }
    }
}